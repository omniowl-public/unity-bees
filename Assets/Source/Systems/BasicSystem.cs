﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using Random = Unity.Mathematics.Random;

[BurstCompile]
public class MoveBeesSystem : SystemBase
{
    private uint randomSeed;

    protected override void OnCreate()
    {
        base.OnCreate();
    }

    protected override void OnUpdate()
    {
        float deltaTime = Time.DeltaTime;
        Random rnd = new Random();
        rnd.InitState(++randomSeed);
        BeeBoundariesData _boundaries = GetSingleton<BeeBoundariesData>();
        Entities.ForEach((Entity entity, ref BeeData bee, ref Translation translation) =>
        {
            if (Vector3.SqrMagnitude(bee.Destination - translation.Value) < 0.001)
            {
                float3 randomLocation = rnd.NextFloat3(_boundaries.LowerLeftCorner, _boundaries.UpperRightCorner);
                bee.Destination = randomLocation;
            }
            else
            {
                float3 newLocation = translation.Value;
                newLocation += math.normalize(bee.Destination - translation.Value) * bee.Speed * deltaTime;
                translation.Value = newLocation;
            }
        }).Schedule();
    }
}