﻿using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.Rendering;
using Random = Unity.Mathematics.Random;

public class BeeSpawner : MonoBehaviour
{
    // Public variables
    [Header("Setup")]
    public MaterialCollection BeeMaterials;
    [Tooltip("How many Bees can be spawned at any one point.")]
    public int BeeCount = 100;
    [Tooltip("Where the bee spawns in global space.")]
    public BeeSpawnGizmo BeeSpawnPosition;
    [Tooltip("The Uniform scale value of all the bees.")]
    public float BeeScale = 1;
    [Tooltip("The minimum amount of energy a Bee can have. This determines how long it can stay out to get pollen.")]
    public float BeeMinEnergy = 1f;
    [Tooltip("The maximum amount of energy a Bee can have. This determines how long it can stay out to get pollen.")]
    public float BeeMaxEnergy = 2f;
    [Tooltip("The bee energy consumption rate.")]
    public float BeeConsumptionPerSecond = 0.0125f;
    [Tooltip("How much Pollen a bee can hold minimum.")]
    public float BeeMinPollen = 0f;
    [Tooltip("How much Pollen a bee can hold maximum.")]
    public float BeeMaxPollen = 1f;
    [Tooltip("The lower this time, the faster the bee is at getting pollen.")]
    public float BeeMinGatheringTime = 0f;
    [Tooltip("The higher this time, the slower the bee is at getting pollen.")]
    public float BeeMaxGatheringTime = 0f;
    [Tooltip("The speed of the bee")]
    public float BeeSpeed = 10f;
    [Header("Rendering")]
    [Tooltip("The Mesh for the bee.")]
    public Mesh BeeMesh;
    [Tooltip("The Material for the bee.")]
    public Material BeeMaterial;
    // Private variables
    private EntityManager m_EntityManager;
    private float3 LowerLeftCorner;
    private float3 UpperRightCorner;

    public void Awake()
    {
        m_EntityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
    }

    // Start is called before the first frame update
    void Start()
    {
        NativeArray<Entity> boundaryArray = m_EntityManager.CreateEntityQuery(typeof(BeeBoundariesData)).ToEntityArray(Allocator.TempJob);
        BeeBoundariesData boundaries = m_EntityManager.GetComponentData<BeeBoundariesData>(boundaryArray[0]);
        LowerLeftCorner = boundaries.LowerLeftCorner;
        UpperRightCorner = boundaries.UpperRightCorner;
        boundaryArray.Dispose();

        Random rnd = new Random();
        rnd.InitState();
        NativeArray<Entity> entityArray = new NativeArray<Entity>(BeeCount, Allocator.Temp);
        EntityArchetype beeType = m_EntityManager.CreateArchetype(
            typeof(BeeData),
            typeof(RenderMesh),
            typeof(RenderBounds),
            typeof(Translation),
            typeof(Rotation),
            typeof(Scale),
            typeof(LocalToWorld));
        m_EntityManager.CreateEntity(beeType, entityArray);
        for (int index = 0; index < entityArray.Length; index++)
        {
            Entity entity = entityArray[index];
            float newBeeMaxEnergy = rnd.NextFloat(BeeMinEnergy, BeeMaxEnergy);
            float newBeeMaxPollen = rnd.NextFloat(BeeMinPollen, BeeMaxPollen);
            float newBeeGatheringTime = rnd.NextFloat(BeeMinGatheringTime, BeeMaxGatheringTime);
            m_EntityManager.SetComponentData(entity, new BeeData()
            {
                Speed = BeeSpeed,
                Energy = newBeeMaxEnergy,
                MaxEnergy = newBeeMaxEnergy,
                EnergyConsumptionRate = BeeConsumptionPerSecond,
                Pollen = 0,
                MaxPollen = newBeeMaxPollen,
                GatheringTime = newBeeGatheringTime,
                IsFlying = true,
                Destination = rnd.NextFloat3(LowerLeftCorner, UpperRightCorner)
            });
            m_EntityManager.SetSharedComponentData(entity, new RenderMesh()
            {
                castShadows = ShadowCastingMode.Off,
                mesh = BeeMesh,
                material = BeeMaterial// BeeMaterials.Materials[rnd.NextInt(0, BeeMaterials.Materials.Length - 1)]
            });

            float3 randomPos = rnd.NextFloat3(LowerLeftCorner, UpperRightCorner);
            m_EntityManager.SetComponentData(entity, new Translation()
            {
                Value = randomPos
            });
            m_EntityManager.SetComponentData(entity, new Rotation()
            {
                Value = quaternion.LookRotation(BeeSpawnPosition.transform.forward, BeeSpawnPosition.transform.up)
            });
            m_EntityManager.SetComponentData(entity, new Scale()
            {
                Value = BeeScale
            });
        }
        entityArray.Dispose();
    }

    public void OnValidate()
    {
        if (BeeMinEnergy >= BeeMaxEnergy)
        {
            BeeMaxEnergy = BeeMinEnergy + 0.1f;
        }

        if (BeeMinPollen >= BeeMaxPollen)
        {
            BeeMaxPollen = BeeMinPollen + 0.1f;
        }

        if (BeeMinGatheringTime >= BeeMaxGatheringTime)
        {
            BeeMaxGatheringTime = BeeMinGatheringTime + 0.1f;
        }
    }
}
