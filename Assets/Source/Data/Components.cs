﻿using Unity.Entities;

public struct FlowerData : IComponentData
{
    /// <summary>
    /// How much Pollen this flower holds.
    /// </summary>
    public float Pollen;
}