﻿using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

[GenerateAuthoringComponent]
public struct BeeBoundariesData : IComponentData
{
    public float3 LowerLeftCorner;
    public float3 UpperRightCorner;
}
