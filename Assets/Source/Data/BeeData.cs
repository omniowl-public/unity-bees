﻿using Unity.Entities;
using Unity.Mathematics;


public struct BeeData : IComponentData
{
    /// <summary>
    /// The flower that the bee is currently going for.
    /// </summary>
    public Entity TargetFlower;
    /// <summary>
    /// The Speed for the bees.
    /// </summary>
    public float Speed;
    /// <summary>
    /// How much energy the bee has left, before it's forced to return home.
    /// </summary>
    public float Energy;
    /// <summary>
    /// How much energy the bee has when it starts flying.
    /// </summary>
    public float MaxEnergy;
    /// <summary>
    /// How much energy the bee loses as it flies per second.
    /// </summary>
    public float EnergyConsumptionRate;
    /// <summary>
    /// How much Pollen the bee is holding right now.
    /// </summary>
    public float Pollen;
    /// <summary>
    /// How much pollen a bee can hold.
    /// </summary>
    public float MaxPollen;
    /// <summary>
    /// How much time it takes for a bee to fill up its Pollen value.
    /// </summary>
    public float GatheringTime;
    /// <summary>
    /// Whether a bee is flying or not right now.
    /// </summary>
    public bool IsFlying;

    public float3 Destination;
}