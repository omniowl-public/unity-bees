﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

/// <summary>
/// This gizmo is a helper to show which direction is forward and up for the bee spawn point.
/// </summary>
public class BeeSpawnGizmo : MonoBehaviour
{
#if UNITY_EDITOR
    [Tooltip("The color of the forward ray.")]
    public Color ForwardGizmoColor = Color.green;
    [Tooltip("The color of the forward ray.")]
    public Color UpGizmoColor = Color.red;
    [Tooltip("The color of the sphere gizmo.")]
    public Color SphereGizmoColor = Color.cyan;
    [Tooltip("The radius of the sphere gizmo")]
    [Range(0.125f, 0.5f)]
    public float SphereGizmoRadius = 0.125f;

    public void OnDrawGizmos()
    {
        Gizmos.color = ForwardGizmoColor;
        Gizmos.DrawRay(transform.position, transform.forward);
        Gizmos.color = UpGizmoColor;
        Gizmos.DrawRay(transform.position, transform.up);
        Gizmos.color = SphereGizmoColor;
        Gizmos.DrawSphere(transform.position, SphereGizmoRadius);
    }
#endif
}
