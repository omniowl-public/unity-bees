﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
[CreateAssetMenu]
public class MaterialCollection : ScriptableObject
{
    [SerializeField]
    public Material[] Materials;
}